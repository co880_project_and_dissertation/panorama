# README #

### Panorama ###

Panorama is an application developed as a feature to be added to Western Front AR. It implements a system based on the jMonkey Engine, that allows historic panoramic photographs to be viewed against the contemporary landscape. It allows the user to adjust both the transparency of the image and its translation. As it will be integrated in to another application, the panoramic image file name is hardcoded as it will ultimately use the Western Front AR database.

A demo video of the software is available by following the link to [this dropbox share](https://www.dropbox.com/sh/cpqfw3cqidt0e4i/AAA0vF9tdp1QGtKvYKhWFuLYa?dl=0)

### How do I get set up? ###

* Clone the repository 

* Open the project using Android Studio

* Compile the project

* Choose to run the application on physical or emulated hardware.