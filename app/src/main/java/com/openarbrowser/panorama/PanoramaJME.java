package com.openarbrowser.panorama;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.TextureKey;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;


/*There are many versions of the Nifty dependency javax.annotation but only one is suitable
//http://blog.anthavio.net/2013/11/how-many-javaxannotation-jars-is-out.html
//Have used version JSR305 through Maven from repository com.google.code.findbugs:jsr305:3.0.0
*/


public class PanoramaJME extends SimpleApplication {

    private static final String TAG = "PanoramaJME";


    // The geometry which will represent the video background
    private Geometry mVideoBGGeom;
    private Geometry fgGeom;
    private float fgGeomWidth = 8, fgGeomHeight = 1;
    // The material which will be applied to the video background geometry.
    private Material mvideoBGMat;
    // The texture displaying the Android camera preview frames.
    private Texture2D mCameraTexture;
    //the User position which serves as intermediate storage place for the Android
    //Location listener position update
    private Vector3f mUserPosition;

    // the JME image which serves as intermediate storage place for the Android
    // camera frame before the pixels get uploaded into the texture.
    private Image mCameraImage;

    // A flag indicating if the scene has been already initialized.
    private boolean mSceneInitialized = false;

    //the User rotation which serves as intermediate storage place for the Android
    //Sensor listener motion update
    private Quaternion mCurrentCamRotationFused;
    private Quaternion mCurrentCamRotation;

    Camera fgCam;
    private float mForegroundCamFOVY = 70; // for a Samsung Galaxy SII

    private Quaternion mInitialCamRotation;
    private Quaternion mRotXQ;
    private Quaternion mRotYQ;
    private Quaternion mRotZQ;
    private Quaternion mRotXYZQ;

    private AnalogListener analogListener = new AnalogListener() {
        float x = fgGeomWidth;
        float alpha = 0.9f;

        @Override
        public void onAnalog(String name, float value, float tpf) {
            if(name.equals("Right")){
                x += value * tpf * 200;
                if(x > fgGeomWidth * 1.9f) {
                    x = fgGeomWidth * 1.9f;
                }
                fgGeom.setLocalTranslation(/*x*/-0.5f * x, /*y*/-0.5f * fgGeomHeight,/*z*/ 0.f);
                //fgGeom.setLocalTranslation(fgGeomWidth + value,fgGeomHeight,0.f);
            }else if(name.equals("Left")){
                x -= value * tpf * 200;
                if(x < fgGeomWidth * 0.1f){
                    x = fgGeomWidth * 0.1f;
                }
                fgGeom.setLocalTranslation(/*x*/-0.5f * x, /*y*/-0.5f * fgGeomHeight,/*z*/ 0.f);
                //fgGeom.setLocalTranslation(fgGeomWidth + value,fgGeomHeight,0.f);
            }
            else if(name.equals("Up")){
                alpha += value * tpf * 50;
                if(alpha > 1.0f){
                    alpha = 1.0f;
                }
                //Set to RGB white. Alter the Alpha value adjust transparency
                ColorRGBA colorRGBA = new ColorRGBA(1f,1f,1f,alpha);
                fgGeom.getMaterial().setColor("Color",colorRGBA);
            }else if(name.equals("Down")){
                alpha -= value * tpf * 50;
                if(alpha < 0.f){
                    alpha = 0.f;
                }
                //Set to RGB white. Alter the Alpha value adjust transparency
                ColorRGBA colorRGBA = new ColorRGBA(1f,1f,1f,alpha);
                fgGeom.getMaterial().setColor("Color",colorRGBA);
            }

        }
    };

    public static void main(String[] args){
        PanoramaJME app = new PanoramaJME();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        // Do not display statistics
        setDisplayStatView(false);
        setDisplayFps(false);

        //http://wiki.jmonkeyengine.org/doku.php/jme3:advanced:input_handling
        inputManager.deleteMapping(SimpleApplication.INPUT_MAPPING_MEMORY);
        inputManager.addMapping("Left", new MouseAxisTrigger(MouseInput.AXIS_X, false),
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // Press and drag left
        inputManager.addMapping("Right", new MouseAxisTrigger(MouseInput.AXIS_X, true),
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); //Press and drag right
        inputManager.addMapping("Down", new MouseAxisTrigger(MouseInput.AXIS_Y, false),
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT) ); // Press and drag down
        inputManager.addMapping("Up", new MouseAxisTrigger(MouseInput.AXIS_Y,true),
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); //Press and drag up
        inputManager.addListener(analogListener, new String[]{"Left", "Right", "Up", "Down"});

        // we use our custom viewports - so the main viewport does not need the  rootNode
        viewPort.detachScene(rootNode);
        initVideoBackground(settings.getWidth(), settings.getHeight());
        //before loading POI objects which may not be available.
        initForegroundScene();
        //initDefaultCamera();
        initBackgroundCamera();
        initForegroundCamera(mForegroundCamFOVY);
        mSceneInitialized = true;
    }


    private void initDefaultCamera(){
        //Default rotation towards origin i.e -z axis
        cam.setLocation(new Vector3f(0f, 0f, 0f));
    }

    /**
     * This function creates the geometry, the viewport and the virtual camera
     * needed for rendering the incoming Android camera frames in the scene
     * graph
     * @param screenWidth
     *                  Width of the screen
     * @param screenHeight
     *                  Height of the screen
     */
    public void initVideoBackground(int screenWidth, int screenHeight) {
        // Create a Quad shape.
        Quad videoBGQuad = new Quad(1, 1, false);
        // Quad videoBGQuad = new Quad(1, 1, true);
        // Create a Geometry with the Quad shape
        mVideoBGGeom = new Geometry("quad", videoBGQuad);
        float newWidth = 1.f * screenWidth / screenHeight;
        // Center the Geometry in the middle of the screen.
        mVideoBGGeom.setLocalTranslation(-0.5f * newWidth, -0.5f, 0.f);//
        // Scale (stretch) the width of the Geometry to cover the whole screen
        // width.
        mVideoBGGeom.setLocalScale(1.f * newWidth, 1.f, 1);
        // Apply a unshaded material which we will use for texturing.
        mvideoBGMat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        //mvideoBGMat.setColor("Color",ColorRGBA.Blue);
        Texture tex = assetManager.loadTexture(new TextureKey("Lighthouse.jpg"));
        tex.setWrap(Texture.WrapMode.Repeat);
        mvideoBGMat.setTexture("ColorMap", tex);
        mVideoBGGeom.setMaterial(mvideoBGMat);
        // Create a new texture which will hold the Android camera preview frame
        // pixels.
        //mCameraTexture = new Texture2D();
        //mUserPosition=new Vector3f();
        mSceneInitialized = true;

        mCurrentCamRotationFused = new Quaternion(0.f,0.f,0.f,1.f);
        mCurrentCamRotation = new Quaternion(0.f,0.f,0.f,1.f);
    }

    @Override
    public void simpleUpdate(float tpf) {
        mVideoBGGeom.updateLogicalState(tpf);
        mVideoBGGeom.updateGeometricState();
    }

    /**
     * Initialises background camera, viewport and geometry.
     */
    public void initBackgroundCamera() {
        // Create a custom virtual camera with orthographic projection
        Camera videoBGCam = cam.clone();
        videoBGCam.setParallelProjection(true);
        // Also create a custom viewport.
        ViewPort videoBGVP = renderManager.createMainView("VideoBGView",
                videoBGCam);
        // Attach the geometry representing the video background to the
        // viewport.
        videoBGVP.attachScene(mVideoBGGeom);
    }

    /**
     * Initialises ninja. Called on application startup by simpleInitApp.
     * May be removed as POI objects cannot be loaded until location data
     * is available.
     */
    public void initForegroundScene() {

        //Set to RGB white. Alter the Alpha value adjust transparency
        ColorRGBA colorRGBA = new ColorRGBA(1f,1f,1f,0.9f);

        //http://hub.jmonkeyengine.org/t/quad-using-unshaded-material-and-texture-with-fully-transparent-area/28053/11
        Quad b = new Quad(fgGeomWidth,fgGeomHeight,false);
        fgGeom = new Geometry("Box", b);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        //mat.setColor("Color", ColorRGBA.White);
        mat.setColor("Color", colorRGBA);
        //mat.setTexture("ColorMap",assetManager.loadTexture("Lighthouse.jpg"));
        //Texture tex = assetManager.loadTexture("Lighthouse.jpg");
        //Texture tex = assetManager.loadTexture(new TextureKey("Lighthouse.jpg"));
        Texture tex = assetManager.loadTexture(new TextureKey("loos_panorama_resize2.jpg"));
        tex.setWrap(Texture.WrapMode.Repeat);
        mat.setTexture("ColorMap", tex);
        mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        fgGeom.setMaterial(mat);
        fgGeom.scale(1.f, 1.f, 1.f);
        fgGeom.rotate(/*x*/0.0f,/*y*/ 0.0f,/*z*/ 0.0f);
        //Must centre geometry
        fgGeom.setLocalTranslation(/*x*/-0.5f * fgGeomWidth, /*y*/-0.5f * fgGeomHeight,/*z*/ 0.f);
        rootNode.attachChild(fgGeom);

        /*Box b = new Box(14,14,14);
        Geometry geom = new Geometry("Box", b);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        geom.setMaterial(mat);
        geom.scale(0.10f, 0.10f, 0.10f);
        geom.rotate(0.0f, 0.0f, 0.0f);
        geom.setLocalTranslation(0.0f, 0.0f, -3.0f);
        rootNode.attachChild(geom);*/

        /*Quad b2 = new Quad(width,height,true);
        Geometry geom2 = new Geometry("Box", b2);
        geom2.setLocalTranslation(-1.2f,-0.5f * height,0.5f * width );
        Material mat2 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat2.setColor("Color", ColorRGBA.Green);
        geom2.rotate(0.0f,1.57f, 0.0f);
        geom2.scale(1.0f, 1.0f, 1.0f);
        geom2.setMaterial(mat2);
        rootNode.attachChild(geom2);

        Quad b3 = new Quad(width,height,true);
        Geometry geom3 = new Geometry("Box", b3);
        geom3.setLocalTranslation(1.2f,-0.5f * width,-0.5f * height);
        Material mat3 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat3.setColor("Color", ColorRGBA.Red);
        geom3.setMaterial(mat3);
        geom3.rotate(0.0f, -1.57f, 0.0f);
        geom3.scale(1.0f, 1.0f, 1.0f);
        rootNode.attachChild(geom3);

        Quad b4 = new Quad(width,height,true);
        Geometry geom4 = new Geometry("Box", b4);
        geom4.setLocalTranslation(0.5f * width,-0.5f * height,1.2f);
        Material mat4 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat4.setColor("Color", ColorRGBA.Cyan);
        geom4.setMaterial(mat4);
        geom4.rotate(0.0f, 3.14f,0.0f);
        geom4.scale(1.0f, 1.0f, 1.0f);
        rootNode.attachChild(geom4);

        Quad b5 = new Quad(width,height,true);
        Geometry geom5 = new Geometry("Box", b5);
        geom5.setLocalTranslation(-0.5f * width,1.2f,-0.5f * height);
        Material mat5 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat5.setColor("Color", ColorRGBA.Magenta);
        geom5.setMaterial(mat5);
        geom5.rotate(1.57f,0.0f, 0.0f);
        geom5.scale(1.0f, 1.0f, 1.0f);
        rootNode.attachChild(geom5);

        Quad b6 = new Quad(width,height,true);
        Geometry geom6 = new Geometry("Box", b6);
        geom6.setLocalTranslation(-0.5f * width,-1.2f,0.5f * height);
        Material mat6 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat6.setColor("Color", ColorRGBA.Orange);
        geom6.rotate(-1.57f, 0.0f,0.0f);
        geom6.setMaterial(mat6);
        geom6.scale(1.0f, 1.0f, 1.0f);
        rootNode.attachChild(geom6);*/

        // You must add a light to make the model visible
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
        sun.setDirection(new Vector3f(1.0f, 0.7f, 0.1f));
        rootNode.addLight(sun);

    }

    /**
     * Initialises the camera used to render foreground objects, including point of interest
     * @param fovY
     *            Camera field of view
     */
    public void initForegroundCamera(float fovY) {

        //fgCam = new Camera(settings.getWidth(), settings.getHeight());
        //fgCam.setLocation(new Vector3f(0f, 0f, 0f));
        //fgCam.setAxes(new Vector3f(-1f, 0f, 0f), new Vector3f(0f, 1f, 0f), new Vector3f(0f, 0f, -1f));
        fgCam = cam.clone();
        fgCam.setParallelProjection(true);

        mInitialCamRotation = new Quaternion();
        mInitialCamRotation.fromAxes(new Vector3f(-1f, 0f, 0f), new Vector3f(0f, 1f, 0f), new Vector3f(0f, 0f, -1f));

        mRotXQ = new Quaternion();
        mRotYQ = new Quaternion();
        mRotZQ = new Quaternion();
        mRotXYZQ = new Quaternion();

        //fgCam.setFrustumPerspective(fovY, settings.getWidth() / settings.getHeight(), 0.5f, 50000);
        ViewPort fgVP = renderManager.createMainView("ForegroundView", fgCam);
        fgVP.attachScene(rootNode);
        fgVP.setClearFlags(false, true, false);
        fgVP.setBackgroundColor(ColorRGBA.Blue);
    }

}
